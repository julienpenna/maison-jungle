import Image from "next/image";
import logo from "../assets/logo.png";

export function Banner() {
  const title = "La maison jungle";

  return (
    <div className="lmj-banner flex align-center p-4">
      <Image
        src={logo}
        width={45}
        height={45}
        alt="logo La maison jungle"
        style={{ width: "auto", height: "auto" }}
      />
      <h1>{title.toUpperCase()}</h1>
    </div>
  );
}
