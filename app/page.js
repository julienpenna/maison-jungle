import { Banner } from "./components/Banner";
import { Cart } from "./components/Cart";

export default function Home() {
  return (
    <div>
      <Banner />
      <Cart />
    </div>
  );
}
